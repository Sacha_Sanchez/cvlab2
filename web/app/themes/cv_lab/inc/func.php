<?php
function asset($chemin){
    return get_template_directory_uri().'/asset'.$chemin;
}
function img(string $chemin):string {
    return get_template_directory_uri().'/asset/img'.$chemin;
}
function path(string $slug=''):string {
    return esc_url(home_url($slug));
}

function debug($var, $height = 200, $fixed = false)
{
    $backt = debug_backtrace()[0];
    if($fixed) {
        echo '<pre style="position: fixed;top:0;left:0;right:0;height:'.$height.'px;z-index:999999;overflow-y: scroll;font-size:.8em;padding: 10px 10px 10px 220px; font-family: Consolas, Monospace; background-color: #000; color: #fff;">';
    } else {
        echo '<pre style="height:'.$height.'px;z-index:999999;overflow-y: scroll;font-size:.8em;padding: 10px 10px 10px 10px; font-family: Consolas, Monospace; background-color: #000; color: #fff;">';
    }
    echo '<p style="font-size:.85rem;">'.$backt['file'].' - '.$backt['line'].'</p>';
    print_r($var);
    echo '</pre>';
}

function web_r($meta,$key){
    if(!empty($meta[$key][0])){
        return $meta[$key][0];
    }
    return '';
}
function getImageOneByPostId($id_post,$size,$alt='',$class=''){
    $url= get_the_post_thumbnail_url($id_post,$size);
    if($url!= ''){
        return '<img src="'.$url.'" alt="'.$alt.'" class="'.$class.'">';
    }
    return '';
}

function getImageById($id_media, string $size, string $alt = '', string $class = ''): string
{
    $img = wp_get_attachment_image_src($id_media,$size);
    if(!empty($img[0])) {
        return '<img src="' . $img[0] . '" alt="' . $alt . '" class="'. $class . '" />';
    }
    return '';
}

function getReseau($meta,$key,$class){
    if(!empty($meta[$key][0])){
        return '<li><a target="_blank" href="'.$meta[$key][0].'"><div class="'.$class.'"></div></a></li>';
    }else{
        return '';
    }
}

function return_objet(){
    $obj=get_queried_object();
    return $obj;
}
function showJson($data){
    header('Content-type: application/json');
    $json = json_encode($data);
    if($json){
        die($json);
    }else {
        die('Error in json encoding');
    }}