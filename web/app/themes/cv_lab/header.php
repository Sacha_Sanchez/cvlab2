<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cv_lab
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Share+Tech&display=swap" rel="stylesheet">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<header id="masthead" class="site-header">
    <div class="wrap">
        <div class="head">
            <a href="<?php echo path() ?>"><img src="<?= img('/logo.svg'); ?>" alt=""></a>
            <nav>
                <ul>
                    <li><a href="">Faire un CV</a></li>
                    <li><a href="">Modèles</a></li>
                    <li><a href="<?php echo path('contact') ?>">Nous contacter</a></li>
                    <li><a href="">Se connecter</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="wrap2">
        <h1>Rédiger un CV et le télécharger immédiatement.<br> En moins de 15 minutes.</h1>
        <div class="buttonHover">
            <a href="">Créer un CV</a>
        </div>
        <p>Et offrez-vous 65% de chance supplémentaire pour décrocher cet emploi</p>
    </div>
</header>
